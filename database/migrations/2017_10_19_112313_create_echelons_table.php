<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEchelonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('echelons', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('supervisor_id')->nullable();
            $table->foreign('supervisor_id')
                ->references('id')->on('echelons');
            $table->string('name');
            $table->string('agency_id');
            $table->foreign('agency_id')
                ->references('id')->on('agencies');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('echelons');
    }
}
