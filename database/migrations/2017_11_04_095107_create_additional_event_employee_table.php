<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalEventEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_event_employee', function (Blueprint $table) {
            $table->string('additional_event_id');
            $table->foreign('additional_event_id')
                ->references('id')->on('additional_events');
            $table->string('employee_id');
            $table->foreign('employee_id')
                ->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_event_employee');
    }
}
