<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAdditionalEventsTableAddAbsenceTypeIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_events', function (Blueprint $table) {
            $table->string('absence_type_id')->nullable();
            $table->foreign('absence_type_id')
                ->references('id')->on('absence_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_events', function (Blueprint $table) {
            $table->dropForeign(['absence_type_id']);
            $table->dropColumn('absence_type_id');
        });
    }
}
