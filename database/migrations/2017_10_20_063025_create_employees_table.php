<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('agency_id');
            $table->foreign('agency_id')
                ->references('id')->on('agencies');
            $table->string('echelon_id')->nullable();
            $table->foreign('echelon_id')
                ->references('id')->on('echelons');
            $table->string('rank_id');
            $table->foreign('rank_id')
                ->references('id')->on('ranks');
            $table->unsignedInteger('workshift_id');
            $table->foreign('workshift_id')
                ->references('id')->on('workshifts');
            $table->unsignedInteger('calendar_id');
            $table->foreign('calendar_id')
                ->references('id')->on('calendars');
            $table->unsignedInteger('religion_id');
            $table->string('allowance_id');
            $table->foreign('allowance_id')
                ->references('id')->on('allowances');
            $table->foreign('religion_id')
                ->references('id')->on('religions');
            $table->string('name');
            $table->enum('gender', ['m', 'f']);
            $table->boolean('married');
            $table->string('phone', 12);
            $table->text('address');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
