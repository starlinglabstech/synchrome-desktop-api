<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAbsenceTypesTableAddAllowancePaidColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absence_types', function (Blueprint $table) {
            $table->boolean('allowance_paid')->default(false);
            $table->boolean('meal_allowance_paid')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absence_types', function (Blueprint $table) {
            $table->dropColumn('allowance_paid');
            $table->dropColumn('meal_allowance_paid');
        });
    }
}
