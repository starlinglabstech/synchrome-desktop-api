<!DOCTYPE html>
<html>
<head>
  <title>Laporan Rinci Kehadiran Pegawai per Periode</title>
  <style>
    body {
      font-family: "Arial", sans-serif;
      font-size: 8px;
    }

    .timestamp {
      font-size: 10px;
    }

    .title {
      font-size: 10px;
      font-weight: bold;
      margin: 0 0 3px 0;
    }

    .agency-name {
      display: block;
      margin: 0 0 3px 0;
    }

    .period {
      display: block;
      margin: 0 0 15px 0;
    }

    .table {
      width: 100%;
      border-collapse: collapse;
    }

    .table thead th {
      text-align: center;
    }

    .table tbody tr td {
      padding: 5px;
      text-align: center;
    }

    .table tbody tr td small {
      display: block;
      margin: 5px 0 0 0;
    }

    .table tbody tr.sub-header td {
      text-align: center;
    }

    .employee {
      width: 100%;
      border: 1px solid #000;
      border-collapse: collapse;
      border-bottom: none;
    }

    .employee tbody tr td {
      border: none;
      padding: 5px;
    }

    .signer {
      width: 100%;
      margin-top: 40px;
    }

    .signer tbody tr td {
      text-align: center;
    }
  </style>
</head>

<body>
  <h1 class="title">LAPORAN RINCI KEHADIRAN PEGAWAI PER PERIODE (RINCI I)</h1>
  <span class="agency-name">{{ $agency->name }}</span>
  <span class="period">Tanggal: {{ $start->format('j F Y') }} s/d {{ $end->format('j F Y') }}</span>
  <table class="employee" border="1">
    <tbody>
      <tr>
        <td width="40">NIP/Nama</td>
        <td width="5">:</td>
        <td><strong>{{ $employee->id }}</strong> - {{ $employee->name }}</td>
      </tr>
      @if(!empty($employee->echelon))
        <tr>
            <td>Jabatan</td>
            <td>:</td>
            <td>{{ $employee->echelon->name }}</td>
        </tr>
      @endif
    </tbody>
  </table>
  <table class="table" border="1">
    <tbody>
      <tr class="sub-header">
        <td rowspan="2" width="15">Hari</td>
        <td rowspan="2" width="35">Tanggal</td>
        <td colspan="2">Jadwal Kerja</td>
        <td colspan="2">Presensi</td>
        <td rowspan="2" width="5">Jam<br>Kerja<br>Pegawai</td>
        <td rowspan="2" width="5">Datang Lambat</td>
        <td colspan="8">Ketidakhadiran</td>
        <td colspan="2">Hadir di Hari</td>
        <td rowspan="2" width="70">Keterangan</td>
      </tr>
      <tr class="sub-header">
        <td width="25">In</td>
        <td width="25">Out</td>
        <td width="25">Datang</td>
        <td width="25">Pulang</td>
        <td width="5">A</td>
        <td width="5">S</td>
        <td width="5">T</td>
        <td width="5">D</td>
        <td width="5">I</td>
        <td width="5">C</td>
        <td width="5">L</td>
        <td width="5">O</td>
        <td width="7.5">Kerja</td>
        <td width="7.5">Libur</td>
      </tr>
      @foreach($data as $row)
        <tr>
          <td>{{ $row['day'] }}</td>
          <td>{{ $row['date'] }}</td>
          <td>{{ $row['workshift_in'] }}</td>
          <td>{{ $row['workshift_out'] }}</td>
          <td>{{ $row['checkin'] }}</td>
          <td>{{ $row['checkout'] }}</td>
          <td>{{ $row['work_duration'] }}</td>
          <td>{{ $row['late_by'] }}</td>
          <td>{{ $row['A'] }}</td>
          <td>{{ $row['S'] }}</td>
          <td>{{ $row['T'] }}</td>
          <td>{{ $row['D'] }}</td>
          <td>{{ $row['I'] }}</td>
          <td>{{ $row['C'] }}</td>
          <td>{{ $row['L'] }}</td>
          <td>{{ $row['O'] }}</td>
          <td>{{ $row['worked'] }}</td>
          <td>{{ $row['off'] }}</td>
          <td>{{ $row['description'] }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <br>
  Ketidakhadiran: @foreach($absence_types as $absence) {{ "$absence->id = $absence->name; " }} @endforeach
  <table class="signer">
    <tbody>
      <tr>
        <td>{{ $signers[0]->echelon->name }}</td>
        <td>{{ $signers[1]->echelon->name }}</td>
      </tr>
      <tr>
        <td height="50px"></td>
        <td height="50px"></td>
      </tr>
      <tr>
        <td>{{ $signers[0]->name }}</td>
        <td>{{ $signers[1]->name }}</td>
      </tr>
      <tr>
        <td>{{ $signers[0]->rank->name }}</td>
        <td>{{ $signers[1]->rank->name }}</td>
      </tr>
      <tr>
        <td>NIP {{ $signers[0]->id }}</td>
        <td>NIP {{ $signers[1]->id }}</td>
      </tr>
    </tbody>
  </table>
</body>
</html>