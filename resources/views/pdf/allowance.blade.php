<!DOCTYPE html>
<html>
<head>
  <title>Laporan Rinci Kehadiran Pegawai per Periode</title>
  <style>
    body {
      font-family: "Arial", sans-serif;
      font-size: 8px;
    }

    .timestamp {
      font-size: 10px;
    }

    .title {
      font-size: 10px;
      font-weight: bold;
      margin: 0 0 3px 0;
    }

    .agency-name {
      display: block;
      margin: 0 0 3px 0;
    }

    .period {
      display: block;
      margin: 0 0 15px 0;
    }

    .table {
      width: 100%;
      border-collapse: collapse;
    }

    .table thead th {
      text-align: center;
    }

    .table tbody tr td {
      padding: 5px;
    }

    .table tbody tr td.number {
      text-align: center;
    }

    .table tbody tr td small {
      display: block;
      margin: 5px 0 0 0;
    }

    .table tbody tr.sub-header td {
      text-align: center;
    }

    .employee {
      width: 100%;
      border: 1px solid #000;
      border-collapse: collapse;
      border-bottom: none;
    }

    .employee tbody tr td {
      border: none;
      padding: 5px;
    }

    .signer {
      width: 100%;
      margin-top: 40px;
    }

    .signer tbody tr td {
      text-align: center;
    }
  </style>
</head>

<body>
  <h1 class="title">DAFTAR TERIMA TUNJANGAN PENINGKATAN KINERJA PNS</h1>
  <span class="agency-name">{{ $agency->name }}</span>
  <span class="period">Tanggal: {{ $start->format('j F Y') }} s/d {{ $end->format('j F Y') }}</span>
  <table class="table" border="1">
    <thead>
      <tr>
        <th rowspan="3">Nama/NIP</th>
        <th rowspan="3" width="100">Eselon<br>Golongan</th>
        <th rowspan="3" width="50">Besaran TPP</th>
        <th colspan="10">Besaran TPP Sesuai Rekap Tingkat Ketidakhadiran</th>
        <th rowspan="3" width="50">Jumlah TPP Bersih</th>
        <th rowspan="3" width="50">Tanda Tangan</th>
      </tr>
      <tr>
        <th rowspan="2" width="15">HK</th>
        <th rowspan="2" width="15">HD</th>
        <th colspan="7">Tidak Hadir</th>
        <th rowspan="2" width="50">Jumlah</th>
      </tr>
      <tr>
        <th width="15">A</th>
        <th width="15">I</th>
        <th width="15">S</th>
        <th width="15">C</th>
        <th width="15">D</th>
        <th width="15">TR</th>
        <th width="15">Jml</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $row)
        <tr>
          <td><strong>{{ $row['name'] }}</strong><br>{{ $row['id'] }}</td>
          <td>{{ $row['allowance']['name'] }}</td>
          <td class="number">{{ number_format($row['allowance']['value']['allowance'], 0) }}</td>
          <td class="number">{{ $row['workdays'] }}</td>
          <td class="number">{{ $row['attendances_in_range'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][0]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][3]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][6]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][1]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][2]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['types'][7]['count'] }}</td>
          <td class="number">{{ $row['absences_in_range']['total'] }}</td>
          <td class="number">{{ number_format($row['allowance']['value']['deduction'], 0) }}</td>
          <td class="number">{{ number_format($row['allowance']['value']['allowance'] - $row['allowance']['value']['deduction'], 0) }}</td>
          <td></td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <table class="signer">
    <tbody>
      <tr>
        <td>{{ $signers[0]->echelon->name }}</td>
        <td>{{ $signers[1]->echelon->name }}</td>
      </tr>
      <tr>
        <td height="50px"></td>
        <td height="50px"></td>
      </tr>
      <tr>
        <td>{{ $signers[0]->name }}</td>
        <td>{{ $signers[1]->name }}</td>
      </tr>
      <tr>
        <td>{{ $signers[0]->rank->name }}</td>
        <td>{{ $signers[1]->rank->name }}</td>
      </tr>
      <tr>
        <td>NIP {{ $signers[0]->id }}</td>
        <td>NIP {{ $signers[1]->id }}</td>
      </tr>
    </tbody>
  </table>
</body>
</html>