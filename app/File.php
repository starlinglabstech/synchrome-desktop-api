<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'path',
    ];

    /**
     * Sanitizes file name.
     * 
     * @return string
     */
    public static function sanitizeName($name)
    {
        $length = strlen($name);

        $duplicates = self::whereRaw('LEFT(name, ?) = ?', [$length, $name])->count();

        if ($duplicates > 0) {
            $name .= '_' . ($duplicates + 1);
        }

        return $name;
    }
}
