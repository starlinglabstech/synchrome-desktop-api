<?php

namespace App\Synchronizers;

use App\Calendar;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CalendarSync extends Sync
{
    public function sync()
    {
        $calendars = $this->fetchCalendars();
        DB::beginTransaction();

        foreach ($calendars as $calendar) {
            $stored_calendar = Calendar::find($calendar->id);

            try {
                if (is_null($stored_calendar)) {
                    $this->createCalendar($calendar);
                } else {
                    $this->checkForCalendarUpdate($stored_calendar, $calendar);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchCalendars()
    {
        $request = $this->client->request('GET', 'calendars', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing calendars');
        }
    }

    protected function createCalendar($data)
    {
        $calendar = Calendar::create([
            'id' => $data->id,
            'name' => $data->name,
            'start' => $data->start,
            'end' => $data->end,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
        $events = [];

        foreach ($data->events as $event) {
            array_push(
                $events,
                new Event([
                    'id' => $event->id,
                    'event_category_id' => $event->event_category_id,
                    'title' => $event->title,
                    'start' => $event->start,
                    'end' => $event->end,
                    'created_at' => $event->created_at,
                    'updated_at' => $event->updated_at,
                    'deleted_at' => $event->deleted_at,
                ])
            );
        }

        $calendar->events()->saveMany($events);
    }

    protected function checkForCalendarUpdate($stored_calendar, $calendar)
    {
        if ($stored_calendar->updated_at->lt(new Carbon($calendar->updated_at))) {
            $stored_calendar->update([
                'name' => $calendar->name,
                'start' => $calendar->start,
                'end' => $calendar->end,
                'created_at' => $calendar->created_at,
                'updated_at' => $calendar->updated_at,
                'deleted_at' => $calendar->deleted_at,
            ]);
        }

        $this->checkEvents($stored_calendar, $calendar);
    }

    protected function checkEvents($stored_calendar, $calendar)
    {
        $stored_calendar->events->each(function ($event) use ($calendar) {
            $updated_event = $this->collect($calendar->events)->where('id', $event->id)->first();

            if (!is_null($updated_event) && $event->updated_at->lt(new Carbon($updated_event->updated_at))) {
                $event->update([
                    'event_category_id' => $updated_event->event_category_id,
                    'title' => $updated_event->title,
                    'start' => $updated_event->start,
                    'end' => $updated_event->end,
                    'created_at' => $updated_event->created_at,
                    'updated_at' => $updated_event->updated_at,
                    'deleted_at' => $updated_event->deleted_at,
                ]);
            }
        });

        $stored_event_ids = $stored_calendar->events->pluck('id')->all();
        $new_events = $this->collect($calendar->events)->whereNotIn('id', $stored_event_ids);
        $new_events = $new_events->map(function ($event) {
            return new Event([
                'id' => $event->id,
                'title' => $event->title,
                'start' => $event->start,
                'end' => $event->end,
                'created_at' => $event->created_at,
                'updated_at' => $event->updated_at,
                'deleted_at' => $event->deleted_at,
                'event_category_id' => $event->event_category_id,
            ]);
        });
        $stored_calendar->events()->saveMany($new_events);
    }
}
