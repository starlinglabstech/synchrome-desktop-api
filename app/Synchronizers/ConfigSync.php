<?php

namespace App\Synchronizers;

use App\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ConfigSync extends Sync
{
    public function sync()
    {
        $configs = $this->fetchConfigs();
        DB::beginTransaction();

        foreach ($configs as $config) {
            $stored_config = Config::find($config->id);

            try {
                if (is_null($stored_config)) {
                    $this->createConfig($config);
                } else {
                    $this->checkForConfigUpdate($stored_config, $config);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchConfigs()
    {
        $request = $this->client->request('GET', 'configs', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing configs');
        }
    }

    protected function createConfig($data)
    {
        $config = Config::create([
            'id' => $data->id,
            'value' => $data->value,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForConfigUpdate($stored_config, $data)
    {
        if (!$stored_config->updated_at->lt(new Carbon($data->updated_at))) {
            return;
        }

        $stored_config->update([
            'id' => $data->id,
            'value' => $data->value,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }
}
