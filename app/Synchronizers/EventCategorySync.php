<?php

namespace App\Synchronizers;

use App\EventCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EventCategorySync extends Sync
{
    public function sync()
    {
        $event_categories = $this->fetchEventCategories();
        DB::beginTransaction();

        foreach ($event_categories as $event_category) {
            $stored_event_category = EventCategory::find($event_category->id);

            try {
                if (is_null($stored_event_category)) {
                    $this->createEventCategory($event_category);
                } else {
                    $this->checkForEventCategoryUpdate($stored_event_category, $event_category);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchEventCategories()
    {
        $request = $this->client->request('GET', 'event-categories', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing event categories');
        }
    }

    protected function createEventCategory($data)
    {
        $agency = EventCategory::create([
            'id' => $data->id,
            'name' => $data->name,
            'color' => $data->color,
            'text_color' => $data->text_color,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForEventCategoryUpdate($stored_event_category, $event_category)
    {
        if (!$stored_event_category->updated_at->lt(new Carbon($event_category->updated_at))) {
            return;
        }

        $stored_event_category->update([
            'name' => $event_category->name,
            'color' => $event_category->color,
            'text_color' => $event_category->text_color,
            'created_at' => $event_category->created_at,
            'updated_at' => $event_category->updated_at,
            'deleted_at' => $event_category->deleted_at,
        ]);
    }
}
