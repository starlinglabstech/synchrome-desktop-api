<?php

namespace App\Synchronizers;

use App\AdditionalEvent;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdditionalEventSync extends Sync
{
    public function sync()
    {
        $fetched_additional_events = $this->fetchAdditionalEvents();
        $stored_additional_events = AdditionalEvent::with('employees')->get();
        $updated_additional_events = $this->filterUpdatedAdditionalEvents($fetched_additional_events, $stored_additional_events)
            ->map(function ($additional_event) {
                $data = [
                    'id' => $additional_event->id,
                    'event_category_id' => $additional_event->event_category_id,
                    'title' => $additional_event->title,
                    'start' => $additional_event->start->format('Y-m-d'),
                    'absence_type_id' => $additional_event->absence_type_id,
                    'permit' => $additional_event->permit,
                    'permit_file' => base64_encode(Storage::disk('local')->get('permits/' . $additional_event->permit)),
                    'created_at' => $additional_event->created_at->format('Y-m-d H:i:s'),
                    'updated_at' => $additional_event->updated_at->format('Y-m-d H:i:s'),
                    'employees' => $additional_event->employees
                        ->map(function ($employee) {
                            return $employee->id;
                        })
                        ->all(),
                ];

                if (!is_null($additional_event->end)) {
                    $data['end'] = $additional_event->end->format('Y-m-d');
                }

                if (!is_null($additional_event->deleted_at)) {
                    $data['deleted_at'] = $additional_event->deleted_at->format('Y-m-d');
                }

                return $data;
            });

        try {
            $updated_additional_events->each(function ($additional_event) {
                $config = $this->headers;
                $config['form_params'] = $additional_event;
                $request = $this->client->request('POST', 'additional-events', $config);

                if ($request->getStatusCode() !== 200) {
                    throw new \Exception('Unexpected error while synchronizing additional events');
                }
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function fetchAdditionalEvents()
    {
        $request = $this->client->request('GET', 'additional-events', $this->headers);

        if ($request->getStatusCode() !== 200) {
            throw new \Exception('Unexpected error while synchronizing additional events');
        } else {
            return json_decode((string) $request->getBody());
        }
    }

    protected function filterUpdatedAdditionalEvents($fetched, $stored)
    {
        return $stored->filter(function ($event) use ($fetched) {
            $match = collect($fetched)->where('id', $event->id)->first();

            return is_null($match) || $event->updated_at->gt(
                Carbon::createFromFormat('Y-m-d H:i:s', $match->updated_at)
            );
        });
    }
}
