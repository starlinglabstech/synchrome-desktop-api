<?php

namespace App\Synchronizers;

use App\Rank;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RankSync extends Sync
{
    public function sync()
    {
        $ranks = $this->fetchRanks();
        DB::beginTransaction();

        foreach ($ranks as $rank) {
            $stored_rank = Rank::find($rank->id);

            try {
                if (is_null($stored_rank)) {
                    $this->createRank($rank);
                } else {
                    $this->checkForRankUpdate($stored_rank, $rank);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchRanks()
    {
        $request = $this->client->request('GET', 'ranks', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing ranks');
        }
    }

    protected function createRank($data)
    {
        Rank::create([
            'id' => $data->id,
            'name' => $data->name,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForRankUpdate($stored_rank, $rank)
    {
        if (!$stored_rank->updated_at->lt(new Carbon($rank->updated_at))) {
            return;
        }

        $stored_rank->update([
            'name' => $rank->name,
            'created_at' => $rank->created_at,
            'updated_at' => $rank->updated_at,
            'deleted_at' => $rank->deleted_at,
        ]);
    }
}
