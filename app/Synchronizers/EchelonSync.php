<?php

namespace App\Synchronizers;

use App\Echelon;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EchelonSync extends Sync
{
    public function sync()
    {
        $echelons = $this->fetchEchelons();
        DB::beginTransaction();

        foreach ($echelons as $echelon) {
            $stored_echelon = Echelon::find($echelon->id);

            try {
                if (is_null($stored_echelon)) {
                    $this->createEchelon($echelon);
                } else {
                    $this->checkForEchelonUpdate($stored_echelon, $echelon);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchEchelons()
    {
        $request = $this->client->request('GET', 'echelons', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing echelons');
        }
    }

    protected function createEchelon($data)
    {
        $echelon = Echelon::create([
            'id' => $data->id,
            'supervisor_id' => $data->supervisor_id,
            'agency_id' => $data->agency_id,
            'name' => $data->name,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForEchelonUpdate($stored_echelon, $data)
    {
        if (!$stored_echelon->updated_at->lt(new Carbon($data->updated_at))) {
            return;
        }

        $stored_echelon->update([
            'supervisor_id' => $data->supervisor_id,
            'agency_id' => $data->agency_id,
            'name' => $data->name,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }
}
