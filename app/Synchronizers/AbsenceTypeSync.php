<?php

namespace App\Synchronizers;

use App\AbsenceType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AbsenceTypeSync extends Sync
{
    public function sync()
    {
        $absence_types = $this->fetchAbsenceTypes();
        DB::beginTransaction();

        foreach ($absence_types as $absence_type) {
            $stored_absence_type = AbsenceType::find($absence_type->id);

            try {
                if (is_null($stored_absence_type)) {
                    $this->createAbsenceType($absence_type);
                } else {
                    $this->checkForAbsenceTypeUpdate($stored_absence_type, $absence_type);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchAbsenceTypes()
    {
        $request = $this->client->request('GET', 'absence-types', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing absence types');
        }
    }

    protected function createAbsenceType($data)
    {
        $agency = AbsenceType::create([
            'id' => $data->id,
            'name' => $data->name,
            'allowance_paid' => $data->allowance_paid,
            'meal_allowance_paid' => $data->meal_allowance_paid,
            'file_required' => $data->file_required,
            'parent_id' => $data->parent_id,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForAbsenceTypeUpdate($stored_absence_type, $data)
    {
        if (!$stored_absence_type->updated_at->lt(new Carbon($data->updated_at))) {
            return;
        }

        $stored_absence_type->update([
            'name' => $data->name,
            'allowance_paid' => $data->allowance_paid,
            'meal_allowance_paid' => $data->meal_allowance_paid,
            'file_required' => $data->file_required,
            'parent_id' => $data->parent_id,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }
}
