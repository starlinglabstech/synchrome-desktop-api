<?php

namespace App\Synchronizers;

use Illuminate\Support\Collection;

abstract class Sync
{
    protected $client;

    protected $headers;

    public function __construct($jwt, $client)
    {
        $this->client = $client;
        $this->headers = ['headers' => ['Authorization' => 'Bearer ' . $jwt, 'Accept' => 'application/json']];
    }

    protected function collect($payload)
    {
        return new Collection($payload);
    }

    abstract public function sync();
}
