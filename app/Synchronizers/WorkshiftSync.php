<?php

namespace App\Synchronizers;

use App\Shift;
use App\Workshift;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WorkshiftSync extends Sync
{
    public function sync()
    {
        $workshifts = $this->fetchWorkshifts();
        DB::beginTransaction();

        foreach ($workshifts as $workshift) {
            $stored_workshift = Workshift::find($workshift->id);

            try {
                if (is_null($stored_workshift)) {
                    $this->createWorkshift($workshift);
                } else {
                    $this->checkForWorkshiftUpdate($stored_workshift, $workshift);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchWorkshifts()
    {
        $request = $this->client->request('GET', 'workshifts', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing workshifts');
        }
    }

    protected function createWorkshift($data)
    {
        $workshift = Workshift::create([
            'id' => $data->id,
            'name' => $data->name,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
        $details = [];

        foreach ($data->details as $detail) {
            array_push(
                $details,
                new Shift([
                    'id' => $detail->id,
                    'day' => $detail->day,
                    'in' => $detail->in,
                    'out' => $detail->out,
                    'active' => $detail->active,
                    'created_at' => $detail->created_at,
                    'updated_at' => $detail->updated_at,
                    'deleted_at' => $detail->deleted_at,
                ])
            );
        }

        $workshift->details()->saveMany($details);
    }

    protected function checkForWorkshiftUpdate($stored_workshift, $workshift)
    {
        if ($stored_workshift->updated_at->lt(new Carbon($workshift->updated_at))) {
            $stored_workshift->update([
                'name' => $workshift->name,
                'created_at' => $workshift->created_at,
                'updated_at' => $workshift->updated_at,
                'deleted_at' => $workshift->deleted_at,
            ]);
        }

        $this->checkDetails($stored_workshift, $workshift);
    }

    protected function checkDetails($stored_workshift, $workshift)
    {
        $stored_workshift->details->each(function ($detail) use ($workshift) {
            $updated_detail = $this->collect($workshift->details)->where('id', $detail->id)->first();

            if (!is_null($updated_detail) && $detail->updated_at->lt(new Carbon($updated_detail->updated_at))) {
                $detail->update([
                    'day' => $updated_detail->day,
                    'in' => $updated_detail->in,
                    'out' => $updated_detail->out,
                    'active' => $updated_detail->active,
                    'created_at' => $updated_detail->created_at,
                    'updated_at' => $updated_detail->updated_at,
                    'deleted_at' => $updated_detail->deleted_at,
                ]);
            }
        });

        $stored_detail_ids = $stored_workshift->details->pluck('id')->all();
        $new_details = $this->collect($workshift->details)->whereNotIn('id', $stored_detail_ids);
        $new_details = $new_details->map(function ($detail) {
            return new Shift([
                'id' => $detail->id,
                'day' => $detail->day,
                'active' => $detail->active,
                'created_at' => $detail->created_at,
                'updated_at' => $detail->updated_at,
                'deleted_at' => $detail->deleted_at,
            ]);
        });
        $stored_workshift->details()->saveMany($new_details);
    }
}
