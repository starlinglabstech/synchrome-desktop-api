<?php

namespace App\Synchronizers;

use App\Allowance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AllowanceSync extends Sync
{
    public function sync()
    {
        $this->pushAllowances();

        $allowances = $this->fetchAllowances();
        DB::beginTransaction();

        foreach ($allowances as $allowance) {
            $stored_allowance = Allowance::withTrashed()->find($allowance->id);

            try {
                if (is_null($stored_allowance)) {
                    $this->createAllowance($allowance);
                } else {
                    $this->checkForAllowanceUpdate($stored_allowance, $allowance);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchAllowances()
    {
        $request = $this->client->request('GET', 'allowances', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing allowances');
        }
    }

    protected function createAllowance($data)
    {
        Allowance::create([
            'id' => $data->id,
            'name' => $data->name,
            'allowance' => $data->allowance,
            'meal_allowance' => $data->meal_allowance,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForAllowanceUpdate($stored_allowance, $data)
    {
        if (!$stored_allowance->updated_at->lt(new Carbon($data->updated_at))) {
            return;
        }

        $stored_allowance->update([
            'name' => $data->name,
            'allowance' => $data->allowance,
            'meal_allowance' => $data->meal_allowance,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function pushAllowances()
    {
        $allowances = Allowance::where('is_from_server', false)->withTrashed()->get()->toArray();

        if (count($allowances) === 0) {
            return;
        }

        $config = $this->headers;
        $config['form_params']['allowances'] = $allowances;

        $request = $this->client->request('POST', 'allowances', $config);
        
        if (!$request->getStatusCode() == 200) {
            throw new \Exception('Unexpected error while pushing allowances');
        }
    }
}
