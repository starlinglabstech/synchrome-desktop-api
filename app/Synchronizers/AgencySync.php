<?php

namespace App\Synchronizers;

use App\Agency;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AgencySync extends Sync
{
    public function sync()
    {
        $agencies = $this->fetchAgencies();
        DB::beginTransaction();

        foreach ($agencies as $agency) {
            $stored_agency = Agency::find($agency->id);

            try {
                if (is_null($stored_agency)) {
                    $this->createAgency($agency);
                } else {
                    $this->checkForAgencyUpdate($stored_agency, $agency);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchAgencies()
    {
        $request = $this->client->request('GET', 'agencies', $this->headers);

        if ($request->getStatusCode() == 200) {
            return json_decode((string) $request->getBody());
        } else {
            throw new \Exception('Unexpected error while synchronizing agencies');
        }
    }

    protected function createAgency($data)
    {
        $agency = Agency::create([
            'id' => $data->id,
            'name' => $data->name,
            'agency_type_id' => $data->agency_type_id,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }

    protected function checkForAgencyUpdate($stored_agency, $data)
    {
        if (!$stored_agency->updated_at->lt(new Carbon($data->updated_at))) {
            return;
        }

        $stored_agency->update([
            'name' => $data->name,
            'agency_type_id' => $data->agency_type_id,
            'created_at' => $data->created_at,
            'updated_at' => $data->updated_at,
            'deleted_at' => $data->deleted_at,
        ]);
    }
}
