<?php

namespace App\Synchronizers;

use App\Employee;
use App\Template;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployeeSync extends Sync
{
    public function sync()
    {
        $employees = $this->fetchEmployees();
        DB::beginTransaction();

        foreach ($employees as $employee) {
            $stored_employee = Employee::find($employee->id);

            try {
                if (is_null($stored_employee)) {
                    $this->createEmployee($employee);
                } else {
                    $this->checkForEmployeeUpdate($stored_employee, $employee);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }
    }

    protected function fetchEmployees()
    {
        $request = $this->client->request('GET', 'agencies/' . env('AGENCY_ID') . '/employees', $this->headers);

        if ($request->getStatusCode() == 200) {
            $data = json_decode((string) $request->getBody());

            if (env('IS_SCHOOL')) {
                $first_signer = $this->fetchEmployeeByEchelon(env('FIRST_SIGNER_ECHELON_ID'));
                $second_signer = $this->fetchEmployeeByEchelon(env('SECOND_SIGNER_ECHELON_ID'));

                if (!is_null($first_signer)) {
                    array_push($data, $first_signer);
                }

                if (!is_null($second_signer)) {
                    array_push($data, $second_signer);
                }
            }

            return $data;
        } else {
            throw new \Exception('Unexpected error while synchronizing employees');
        }
    }

    protected function fetchEmployeeByEchelon($echelon_id)
    {
        $request = $this->client->request('GET', 'echelons/' . $echelon_id . '/employee', $this->headers);

        if ($request->getStatusCode() !== 200) {
            return null;
        }

        return json_decode((string) $request->getBody());
    }

    protected function createEmployee($data)
    {
        $employee = new Employee;
        $employee->id = $data->id;
        $employee->pin = Employee::generatePin();
        $employee->agency_id = $data->agency_id;
        $employee->echelon_id = $data->echelon_id;
        $employee->workshift_id = $data->workshift_id;
        $employee->religion_id = $data->religion_id;
        $employee->allowance_id = $data->allowance_id;
        $employee->rank_id = $data->rank_id;
        $employee->calendar_id = $data->calendar_id;
        $employee->name = $data->name;
        $employee->gender = $data->gender;
        $employee->married = $data->married;
        $employee->address = $data->address;
        $employee->phone = $data->phone;
        $employee->created_at = $data->created_at;
        $employee->updated_at = $data->updated_at;
        $employee->deleted_at = $data->deleted_at;
        $employee->save();

        $templates = collect($data->templates)->map(function ($template) {
            return new Template([
                'template' => $template->template,
                'index' => $template->index,
                'alg_ver' => $template->alg_ver,
            ]);
        });
        $employee->templates()->saveMany($templates);
    }

    protected function checkForEmployeeUpdate($stored_employee, $employee)
    {
        if (!$stored_employee->updated_at->lt(new Carbon($employee->updated_at))) {
            return;
        }

        $stored_employee->agency_id = env('AGENCY_ID');
        $stored_employee->echelon_id = $employee->echelon_id;
        $stored_employee->workshift_id = $employee->workshift_id;
        $stored_employee->religion_id = $employee->religion_id;
        $stored_employee->allowance_id = $employee->allowance_id;
        $stored_employee->rank_id = $employee->rank_id;
        $stored_employee->calendar_id = $employee->calendar_id;
        $stored_employee->name = $employee->name;
        $stored_employee->gender = $employee->gender;
        $stored_employee->married = $employee->married;
        $stored_employee->address = $employee->address;
        $stored_employee->phone = $employee->phone;
        $stored_employee->created_at = $employee->created_at;
        $stored_employee->updated_at = $employee->updated_at;
        $stored_employee->deleted_at = $employee->deleted_at;
        $stored_employee->save();

        $templates = collect($data->templates)->map(function ($template) {
            return new Template([
                'template' => $template->template,
                'index' => $template->index,
                'alg_ver' => $template->alg_ver,
            ]);
        });
        $employee->templates()->delete();
        $employee->templates()->saveMany($templates);
    }
}
