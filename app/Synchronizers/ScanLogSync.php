<?php

namespace App\Synchronizers;

use App\ScanLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ScanLogSync extends Sync
{
    public function sync()
    {
        $config = $this->headers;

        ScanLog::get()
            ->chunk(50)
            ->each(function ($scan_logs) use ($config) {
                $config['form_params']['scan_logs'] = $scan_logs->toArray();

                $request = $this->client->request('POST', 'scan-logs', $config);

                if (!$request->getStatusCode() === 200) {
                    throw new \Exception('Unexpected error while pushing scan logs');
                }
            });
    }
}
