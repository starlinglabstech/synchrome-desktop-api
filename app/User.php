<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'role_id', 'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token', 'role_id',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function generateJwt()
    {
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuer(env('APP_URL'))
            ->setAudience(env('APP_URL'))
            ->setIssuedAt(time())
            ->set('user', User::with('role')->find($this->id))
            ->sign($signer, env('JWT_SIGN_KEY'))
            ->getToken();

        return (string) $token;
    }
}
