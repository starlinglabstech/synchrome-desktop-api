<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AbsenceType extends Model
{
    use SoftDeletes;

    /**
     * Casted attributes
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'allowance_paid' => 'boolean',
        'meal_allowance_paid' => 'boolean',
        'file_required' => 'boolean',
    ];

    /**
     * Set primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'allowance_paid',
        'meal_allowance_paid',
        'file_required',
        'parent_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Relationship to AbsenceType (recursive) as a child.
     *
     * @return AbsenceType
     */
    public function parent()
    {
        return $this->belongsTo(AbsenceType::class, 'parent_id');
    }

    /**
     * Relationship to AbsenceType (recursive) as a parent.
     *
     * @return Illuminate\Support\Collection
     */
    public function children()
    {
        return $this->hasMany(AbsenceType::class, 'parent_id');
    }

    /**
     * Chain query as a parent.
     *
     * @return Illuminate\Database\Query\Builder
     */
    public static function parentOnly()
    {
        return self::where('parent_id', null);
    }
}
