<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Allowance extends Model
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string',
        'allowance' => 'double',
        'meal_allowance' => 'double',
        'is_from_server' => 'boolean',
    ];

    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'allowance',
        'meal_allowance',
        'is_from_server',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
