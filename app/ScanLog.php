<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScanLog extends Model
{
    use SoftDeletes;

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
        'allowance_paid' => 'boolean',
        'meal_allowance_paid' => 'boolean',
    ];

    protected $fillable = [
        'employee_id',
        'absence_type_id',
        'date',
        'workshift_in',
        'workshift_out',
        'checkin',
        'checkout',
        'status',
        'work_duration',
        'off_duration',
        'workshift_work_duration',
        'early_by',
        'late_by',
        'allowance_paid',
        'meal_allowance_paid',
    ];

    protected $dates = [
        'deleted_at',
        'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function absenceType()
    {
        return $this->belongsTo(AbsenceType::class);
    }
}
