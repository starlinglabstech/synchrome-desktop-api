<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes;

    protected $casts = ['id' => 'string'];

    public $incrementing = false;

    protected $fillable = [
        'id',
        'agency_type_id',
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function type()
    {
        return $this->belongsTo(AgencyType::class, 'agency_type_id');
    }

    public function echelons()
    {
        return $this->hasMany(Echelon::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
