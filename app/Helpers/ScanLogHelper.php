<?php

namespace App\Helpers;

use App\Employee;
use App\ScanLog;
use Carbon\Carbon;
use DB;
use Dewadg\FmConnector\Connector;
use Illuminate\Support\Collection;

class ScanLogHelper
{
    protected $connector;

    public function __construct()
    {
        $this->connector = new Connector(
            'easy_link',
            [
                'serial_number' => env('DEVICE_SERIAL_NUMBER'),
                'base_uri' => env('DEVICE_HOST'),
            ]
        );
    }

    public function generateScanLogs(Carbon $date = null)
    {
        $employees = Employee::with([
            'workshift',
            'calendar',
            'additionalEvents',
        ])->get();

        try {
            DB::beginTransaction();

            $employees->each(function ($employee) use ($date) {
                if (!$employee->workshift->atDate($date)->active) {
                    return;
                }
                
                $date = is_null($date) ? Carbon::now() : $date;
                $scan_log_exists = !is_null(ScanLog::where(['employee_id' => $employee->id, 'date' => $date->format('Y-m-d')])->first());

                if (!$scan_log_exists) {
                    $workshift_work_duration = (strtotime($employee->workshift->atDate($date)->out) - strtotime($employee->workshift->atDate($date)->in)) / 60;
                    $events = $employee->calendar->eventAt($date)->merge($employee->additionalEvents);

                    if ($events->count() > 0) {
                        $events->each(function ($event) use (
                            $employee,
                            $date,
                            $workshift_work_duration
                        ) {
                            ScanLog::create([
                                'employee_id' => $employee->id,
                                'date' => $date,
                                'workshift_in' => $employee->workshift->atDate($date)->in,
                                'workshift_out' => $employee->workshift->atDate($date)->out,
                                'workshift_work_duration' => $workshift_work_duration,
                                'work_duration' => $workshift_work_duration,
                                'status' => true,
                                'off_duration' => 0,
                                'absence_type_id' => isset($event->absence_type_id) ? $event->absence_type_id : 'O',
                                'description' => $event->title,
                                'allowance_paid' => $event->absenceType->allowance_paid,
                                'meal_allowance_paid' => $event->absenceType->meal_allowance_paid,
                            ]);
                        });
                    } else {
                        ScanLog::create([
                            'employee_id' => $employee->id,
                            'date' => $date,
                            'workshift_in' => $employee->workshift->atDate($date)->in,
                            'workshift_out' => $employee->workshift->atDate($date)->out,
                            'workshift_work_duration' => $workshift_work_duration,
                            'status' => false,
                        ]);
                    }
                }
            });

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    public function fetchByDate($date)
    {
        $scan_logs = new Collection($this->connector->getScanLogs()->getPayload());

        return $scan_logs
            ->filter(function ($scan_log) use ($date) {
                return date_create($scan_log['date_time'])->format('Y-m-d') == $date;
            })
            ->values()
            ->all();
    }

    public function mergeScanLogs(Carbon $date)
    {
        $date = is_null($date) ? Carbon::now() : $date;
        $scan_logs = (new Collection($this->fetchByDate($date->format('Y-m-d'))));
        
        Employee::where(['agency_id' => env('AGENCY_ID')])
            ->get()
            ->each(function ($employee) use ($scan_logs, $date) {
                if ($employee->isOffAt($date)) {
                    return;
                }

                $scan_log = ScanLog::where([
                    'date' => $date->format('Y-m-d'),
                    'employee_id' => $employee->id,
                ])->first();
                $device_scan_logs = array_values($scan_logs->where('user_id', $employee->pin)->all());

                if (! isset($device_scan_logs[0])) {
                    if (! $employee->isOffAt($date) && $employee->overWorkshiftTime()) {
                        $scan_log->update([
                            'absence_type_id' => 'A',
                            'work_duration' => 0,
                            'off_duration' => $scan_log->workshift_work_duration,
                            'allowance_paid' => false,
                            'meal_allowance_paid' => false,
                        ]);
                    }

                    return;
                } else {
                    $workshift_in = strtotime($scan_log->workshift_in);
                    $checkin = strtotime($device_scan_logs[0]['date_time']);
                    $early_by = $checkin < $workshift_in ? ($workshift_in - $checkin) / 60 : 0;
                    $late_by = $workshift_in < $checkin ? ($checkin - $workshift_in) / 60 : 0;

                    if (! isset($device_scan_logs[1])) {
                        // First-time finger scan
                        $scan_log->update([
                            'checkin' => $device_scan_logs[0]['date_time'],
                            'off_duration' => $workshift_in < $checkin ? (($checkin - $workshift_in) / 60) : 0,
                            'early_by' => $early_by,
                            'late_by' => $late_by,
                        ]);
                    } else {
                        // Second-time finger scan
                        $workshift_out = strtotime($scan_log->workshift_out);
                        $checkout = strtotime($device_scan_logs[1]['date_time']);
                        $work_duration = (($checkout - $checkin) / 60);

                        $scan_log->update([
                            'checkin' => $device_scan_logs[0]['date_time'],
                            'checkout' => $device_scan_logs[1]['date_time'],
                            'work_duration' => $work_duration <= $scan_log->workshift_work_duration ? $work_duration : $scan_log->workshift_work_duration,
                            'off_duration' => $scan_log->workshift_work_duration - (($checkout - $checkin) / 60),
                            'status' => true,
                            'early_by' => $early_by,
                            'late_by' => $late_by,
                            'allowance_paid' => true,
                            'meal_allowance_paid' => true,
                        ]);
                    }
                }
            });

        return true;
    }

    public function fetchEmployees()
    {
        return $this->connector->getUsers();
    }
}
