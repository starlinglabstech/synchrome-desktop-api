<?php

namespace App\Helpers;

use App\Employee;
use App\MonthlyAllowance;
use DB;

class AllowanceCalculator
{
	public function generateRows($year, $month)
	{
		Employee::get(['id'])->each(function ($item) use ($year, $month) {
			MonthlyAllowance::create([
				'employee_id' => $item->id,
				'year' => $year,
				'month' => $month,
				'tpp' => 0,
				'meal' => 0
			]);
		});
	}

	public function calculateTpp(Employee $employee, $start, $end)
	{
		$tpp_value = $employee->allowance->tpp;
		$work_time = $employee->scanLogs()		
			->where('status', true)
			->whereBetween('date', [$start, $end])
			->select(DB::raw('SUM(off_duration) AS off_duration'), DB::raw('SUM(workshift_work_duration) AS workshift_work_duration'))
			->first();

		return (double) ($tpp_value - (($work_time->off_duration / $work_time->workshift_work_duration) * $tpp_value));
	}

	public function calculateMealAllowance(Employee $employee, $start, $end)
	{
		$meal_value = $employee->allowance->meal;
		$work_day = $employee->scanLogs()
			->where('status', true)
			->whereBetween('date', [$start, $end])
			->count();

		if ($work_day > 22) {
			return (double) (22 * $meal_value);
		}

		return (double) ($work_day * $meal_value);
	}
}