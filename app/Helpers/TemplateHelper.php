<?php

namespace App\Helpers;

use App\Employee;
use Carbon\Carbon;
use Dewadg\FmConnector\Connector;
use Illuminate\Support\Facades\DB;

class TemplateHelper
{
    protected $connector;

    public function __construct()
    {
        $this->connector = new Connector(
            'easy_link',
            [
                'serial_number' => env('DEVICE_SERIAL_NUMBER'),
                'base_uri' => env('DEVICE_HOST'),
            ]
        );
    }

    public function sync($reset = false)
    {
        if ($reset) {
            $this->connector->destroyUsers();
            DB::statement('UPDATE employees SET last_device_sync = NULL');
            $employees = Employee::with('templates')->get();
        } else {
            $employees = Employee::with('templates')
                ->whereRaw('last_device_sync IS NULL OR updated_at > last_device_sync')
                ->get();
        }

        try {
            $employees->each(function ($employee) use ($reset) {
                if (count($employee->templates) > 0 && (is_null($employee->last_device_sync) || $employee->last_device_sync->lt($employee->updated_at))) {
                    $payload = [
                        'id' => $employee->pin,
                        'name' => substr($employee->name, 0, 15),
                        'password' => '123',
                        'rfid' => 0,
                        'privilege' => 0,
                        'templates' => $employee->templates->map(function ($template) {
                            return [
                                'index' => (int) $template->index,
                                'alg_ver' => (int) $template->alg_ver,
                                'template' => $template->template,
                            ];
                        })->all(),
                    ];

                    $this->connector->storeUser($payload);
                    $employee->update(['last_device_sync' => Carbon::now()]);
                }
            });

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
