<?php

namespace App\Helpers;

use App\Synchronizers\Sync;
use GuzzleHttp\Client;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class SyncHelper
{
    protected $synchronizers = [
        \App\Synchronizers\ConfigSync::class,
        \App\Synchronizers\AgencySync::class,
        \App\Synchronizers\EchelonSync::class,
        \App\Synchronizers\EventCategorySync::class,
        \App\Synchronizers\CalendarSync::class,
        // \App\Synchronizers\AdditionalEventSync::class,
        \App\Synchronizers\AbsenceTypeSync::class,
        \App\Synchronizers\AllowanceSync::class,
        \App\Synchronizers\WorkshiftSync::class,
        \App\Synchronizers\RankSync::class,
        \App\Synchronizers\EmployeeSync::class,
        \App\Synchronizers\ScanLogSync::class,
    ];

    public function sync()
    {
        $jwt = $this->generateJwt();
        $client = new Client([
            'base_uri' => env('SERVER'),
        ]);

        foreach ($this->synchronizers as $synchronizer) {
            $sync = new $synchronizer($jwt, $client);

            print get_class($sync) . PHP_EOL;
            $sync->sync();

            sleep(2);
        }

        return true;
    }

    protected function generateJwt()
    {
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuedAt(time())
            ->set('key', env('CLUSTER_KEY'))
            ->sign($signer, env('EXTERNAL_JWT_SIGN_KEY'))
            ->getToken();

        return (string) $token;
    }
}