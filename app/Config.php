<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use SoftDeletes;

    protected $casts = ['id' => 'string'];

    public $incrementing = false;

    protected $fillable = [
        'id',
        'value',
    ];
}
