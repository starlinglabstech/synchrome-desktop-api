<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalEvent extends Model
{
    use SoftDeletes;

    /**
     * Casted attributes
     *
     * @var array
     */
    protected $casts = ['id' => 'string'];

    /**
     * Sets primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'event_category_id',
        'user_id',
        'title',
        'start',
        'end',
        'absence_type_id',
        'permit',
        'file_id',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'start',
        'end',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'file_id',
    ];

    /**
     * Relationship to User.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship to EventCategory.
     *
     * @return EventCategory
     */
    public function category()
    {
        return $this->belongsTo(EventCategory::class, 'event_category_id');
    }

    /**
     * Relationship to Employee.
     *
     * @return array
     */
    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }

    /**
     * Generates unique ID.
     *
     * @return string
     */
    public static function generateId()
    {
        $id = env('CLUSTER_KEY') . mt_rand();

        if (self::where('id', $id)->count() > 0) {
            return self::generateId();
        }

        return $id;
    }

    /**
     * Relationship to AbsenceType.
     *
     * @return AbsenceType
     */
    public function absenceType()
    {
        return $this->belongsTo(AbsenceType::class, 'absence_type_id');
    }
}
