<?php

namespace App\Http\Controllers;

use App\Workshift;

class WorkshiftController extends Controller
{
    public function index()
    {
        return Workshift::get();
    }

    public function get($id)
    {
        $workshift = Workshift::with('details')->find($id);

        if (is_null($workshift)) {
            return response()->json('workshift_not_found', 404);
        }

        return $workshift;
    }
}
