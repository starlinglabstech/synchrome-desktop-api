<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Agency;
use App\AbsenceType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class ReportController extends Controller
{
    protected $dompdf;

    public function __construct()
    {
        $this->dompdf = app()->make('dompdf.wrapper');
    }

    public function scanLogByEmployee(Request $request, $employee_id)
    {
        $employee = Employee::find($employee_id);

        if (is_null($employee)) {
            abort(404);
        }

        $this->validate($request, [
            'start' => 'required|date',
            'end' => 'required|date'
        ]);

        $start = Carbon::createFromFormat('Y-m-d', $request->get('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->get('end'));
        $agency = Agency::find(env('AGENCY_ID'));
        $absence_types = AbsenceType::get();
        $signers = $this->getSigners();

        $data = $employee->scanLogs()
            ->whereBetween(
                'date',
                [
                    $start->format('Y-m-d'),
                    $end->format('Y-m-d'),
                ]
            )
            ->get()
            ->map(function ($scan_log) {
                return [
                    'day' => $scan_log->date->format('D'),
                    'date' => $scan_log->date->format('Y-m-d'),
                    'workshift_in' => $scan_log->workshift_in,
                    'workshift_out' => $scan_log->workshift_out,
                    'checkin' => !is_null($scan_log->checkin) ? $scan_log->checkin : '-',
                    'checkout' => !is_null($scan_log->checkout) ? $scan_log->checkout : '-',
                    'work_duration' => $scan_log->work_duration,
                    'late_by' => !is_null($scan_log->late_by) ? $scan_log->late_by : '-',
                    'A' => $scan_log->absence_type_id === 'A' ? 1 : '-',
                    'S' => $scan_log->absence_type_id === 'S' ? 1 : '-',
                    'T' => $scan_log->absence_type_id === 'T' ? 1 : '-',
                    'D' => $scan_log->absence_type_id === 'D' ? 1 : '-',
                    'I' => $scan_log->absence_type_id === 'I' ? 1 : '-',
                    'C' => $scan_log->absence_type_id === 'C' ? 1 : '-',
                    'L' => $scan_log->absence_type_id === 'L' ? 1 : '-',
                    'O' => $scan_log->absence_type_id === 'O' ? 1 : '-',
                    'worked' => $scan_log->status ? 'Y' : '-',
                    'off' => !$scan_log->status ? 'Y' : '-',
                    'description' => '',
                ];
            })
            ->groupBy('date')
            ->all();

        for ($i = $start; $start->lte($end); $start->addDay()) {
            if (!isset($data[$i->format('Y-m-d')])) {
                $data[$i->format('Y-m-d')] = [
                    'day' => $i->format('D'),
                    'date' => $i->format('Y-m-d'),
                    'workshift_in' => '-',
                    'workshift_out' => '-',
                    'checkin' => '-',
                    'checkout' =>'-',
                    'work_duration' => '-',
                    'late_by' => '-',
                    'A' => '-',
                    'S' => '-',
                    'T' => '-',
                    'D' => '-',
                    'I' => '-',
                    'C' => '-',
                    'L' => '-',
                    'O' => '-',
                    'worked' => '-',
                    'off' => '-',
                    'description' => '-',
                ];
            } else {
                $data[$i->format('Y-m-d')] = $data[$i->format('Y-m-d')]->all()[0];
            }
        }

        $data = collect($data)
            ->sortBy(function ($row, $key) {
                return strtotime($key);
            })
            ->all();

        if ($request->get('format') !== null && $request->get('format') === 'json') {
            return $data;
        }

        return response()->json([
            'file_name' => 'kehadiran_' . $request->get('start') . '_' . $request->get('end') . '.pdf',
            'content' => base64_encode($this->dompdf->loadView('pdf.scan_log', compact('start', 'end', 'data', 'employee', 'agency', 'absence_types', 'signers'))->output()),
        ]);
    }

    public function allowance(Request $request)
    {
        $start = Carbon::createFromFormat('Y-m-d', $request->get('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->get('end'));
        $agency = Agency::find(env('AGENCY_ID'));
        $signers = $this->getSigners();
        
        $data = Employee::with(['allowance', 'scanLogs'])->get()
            ->map(function ($employee) use ($start, $end) {
                $workdays = $employee->workshift->workdaysInRange($employee->calendar, $start, $end);
                $attendances_in_range = $employee->attendancesInRange($start, $end);
                $absences_in_range = $employee->absencesInRange($start, $end);

                return [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'allowance' => [
                        'name' => $employee->allowance->name,
                        'value' => $employee->allowanceInRange($start, $end),
                    ],
                    'workdays' => $workdays,
                    'attendances_in_range' => $attendances_in_range,
                    'absences_in_range' => $absences_in_range,
                ];
            })
            ->all();

        if ($request->get('format') !== null && $request->get('format') === 'json') {
            return $data;
        }

        $this->dompdf->setPaper('A4', 'landscape');

        return response()->json([
            'file_name' => 'tpp_' . $request->get('start') . '_' . $request->get('end') . '.pdf',
            'content' => base64_encode($this->dompdf->loadView('pdf.allowance', compact('start', 'end', 'agency', 'data', 'signers'))->output()),
        ]);
    }

    public function mealAllowance(Request $request)
    {
        $start = Carbon::createFromFormat('Y-m-d', $request->get('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->get('end'));
        $agency = Agency::find(env('AGENCY_ID'));
        $signers = $this->getSigners();
        
        $data = Employee::with(['allowance', 'scanLogs'])->get()
            ->map(function ($employee) use ($start, $end) {
                $workdays = $employee->workshift->workdaysInRange($employee->calendar, $start, $end);
                $attendances_in_range = $employee->attendancesInRange($start, $end);
                $absences_in_range = $employee->absencesInRange($start, $end);

                return [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'allowance' => [
                        'name' => $employee->allowance->name,
                        'value' => $employee->mealAllowanceInRange($start, $end),
                    ],
                    'workdays' => $workdays,
                    'attendances_in_range' => $attendances_in_range,
                    'absences_in_range' => $absences_in_range,
                ];
            })
            ->all();

        if ($request->get('format') !== null && $request->get('format') === 'json') {
            return $data;
        }

        $this->dompdf->setPaper('A4', 'landscape');

        return response()->json([
            'file_name' => 'uang_makan_' . $request->get('start') . '_' . $request->get('end') . '.pdf',
            'content' => base64_encode($this->dompdf->loadView('pdf.meal_allowance', compact('start', 'end', 'agency', 'data', 'signers'))->output()),
        ]);
    }

    protected function getSigners()
    {
        $first_signer = Employee::with([
                'agency',
                'echelon',
                'rank',
            ])
            ->where('echelon_id', env('FIRST_SIGNER_ECHELON_ID'))
            ->first();

        $second_signer = Employee::with([
                'agency',
                'echelon',
                'rank',
            ])
            ->where('echelon_id', env('SECOND_SIGNER_ECHELON_ID'))
            ->first();

        return [
            $first_signer,
            $second_signer,
        ];
    }
}
