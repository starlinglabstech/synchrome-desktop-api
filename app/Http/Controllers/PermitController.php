<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class PermitController extends Controller
{
    public function get($file_name)
    {
        $file_location = 'permits/' . $file_name;
        try {
            $file = Storage::disk('local')->get($file_location);
        } catch (\Exception $e) {
            return response()->json('file_not_found', 404);
        }

        return response($file, 200)
            ->header('Content-Type', Storage::getMetaData($file_location))
            ->header('Content-Disposition', 'attachment');
    }
}
