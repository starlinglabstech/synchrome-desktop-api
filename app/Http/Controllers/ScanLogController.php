<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Helpers\ScanLogHelper;
use App\ScanLog;
use Carbon\Carbon;

class ScanLogController extends Controller
{
    protected $helper;

    public function __construct()
    {
        $this->helper = new ScanLogHelper;
    }

    public function today()
    {
        return response()->json($this->helper->fetchToday());
    }

    public function generateByDate(Request $request)
    {
        $this->validate(
            $request,
            [
                'start' => 'required|date',
                'end' => 'sometimes|required|date',
            ]
        );
        $user_data = $request->all();

        try {
            if (empty($user_data['end'])) {
                $date = Carbon::createFromFormat('Y-m-d', $user_data['start']);
                $this->helper->generateScanLogs($date);
            } else {
                $start = Carbon::createFromFormat('Y-m-d', $user_data['start']);
                $end = Carbon::createFromFormat('Y-m-d', $user_data['end']);

                for ($current = $start; $current->lte($end); $current->addDay()) {
                    $this->helper->generateScanLogs($current);
                }
            }

            return response()->json();
        } catch (\Exception $e) {
            return response()->json('unable_to_generate_scan_logs', 500);
        }
    }

    public function mergeByDate(Request $request)
    {
        $this->validate(
            $request,
            [
                'start' => 'required|date',
                'end' => 'sometimes|required|date',
            ]
        );
        $user_data = $request->all();

        try {
            if (empty($user_data['end'])) {
                $date = Carbon::createFromFormat('Y-m-d', $user_data['start']);
                $this->helper->mergeScanLogs($date);
            } else {
                $start = Carbon::createFromFormat('Y-m-d', $user_data['start']);
                $end = Carbon::createFromFormat('Y-m-d', $user_data['end']);

                for ($current = $start; $current->lte($end); $current->addDay()) {
                    $this->helper->mergeScanLogs($current);
                }
            }

            return response()->json();
        } catch (\Exception $e) {
            return response()->json('unable_to_merge_scan_logs', 500);
        }
    }

    public function index(Request $request)
    {
        $this->validate(
            $request,
            [
                'date' => 'sometimes|required|date',
            ]
        );
        $user_data = $request->all();

        if (empty($user_data)) {
            return ScanLog::with('employee')
                ->orderBy('date', 'DESC')
                ->get();
        }

        return ScanLog::with('employee')
            ->where('date', $user_data['date'])
            ->orderBy('date', 'DESC')
            ->get();
    }
}