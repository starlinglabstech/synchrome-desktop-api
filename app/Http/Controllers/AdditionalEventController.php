<?php

namespace App\Http\Controllers;

use App\AdditionalEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdditionalEventController extends Controller
{
    public function index()
    {
        return AdditionalEvent::with(['category', 'employees'])->get();
    }

    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'title' => 'required',
                'start' => 'required|date',
                'event_category_id' => 'required',
                'employees' => 'required|array',
            ]
        );

        $user_data['additional_event'] = $request->except('employees');
        $user_data['additional_event']['id'] = AdditionalEvent::generateId();
        $user_data['additional_event']['user_id'] = $request->user->id;
        $user_data['employees'] = $request->get('employees');

        if ($user_data['additional_event']['start'] === $user_data['additional_event']['end']) {
            unset($user_data['additional_event']['end']);
        }

        try {
            if (! is_null($request->get('permit'))) {
                $file = base64_decode($request->get('permit'));
                
                Storage::disk('local')->put($user_data['additional_event']['id'], $file);

                $user_data['additional_event']['permit'] = $user_data['additional_event']['id'];
            }

            DB::transaction(function () use ($user_data) {
                $additional_event = AdditionalEvent::create($user_data['additional_event']);
                $additional_event->employees()->sync($user_data['employees']);
            });

            return response()->json([], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function get($id)
    {
        $additional_event = AdditionalEvent::with([
            'employees',
            'category',
        ])->find($id);

        if (is_null($additional_event)) {
            return response()->json('additional_event_not_found', 404);
        }

        return $additional_event;
    }

    public function update(Request $request, $id)
    {
        $additional_event = AdditionalEvent::find($id);

        if (is_null($additional_event)) {
            return response()->json('additional_event_not_found', 404);
        }

        $this->validate(
            $request, 
            [
                'title' => 'required',
                'start' => 'required|date',
                'event_category_id' => 'required',
                'employees' => 'required|array',
                'absence_type_id' => 'required_if:event_category_id,1',
                'permit' => 'sometimes|required_if:absence_type_id,C,I|file',
            ]
        );

        $user_data['additional_event'] = $request->except('employees');
        $user_data['employees'] = $request->get('employees');

        if (
            !isset($user_data['additional_event']['end']) ||
            $user_data['additional_event']['start'] === $user_data['additional_event']['end']
        ) {
            $user_data['additional_event']['end'] = null;
        }

        try {
            if ($request->hasFile('permit') && $request->file('permit')->isValid()) {
                $file_name = $additional_event->id . '.' . $request->file('permit')->getClientOriginalExtension();

                $request->file('permit')->move(storage_path() . '/app/permits', $file_name);
                $user_data['additional_event']['permit'] = $file_name;
            }

            DB::transaction(function () use ($additional_event, $user_data) {
                $additional_event->update($user_data['additional_event']);
                $additional_event->employees()->sync($user_data['employees']);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        $additional_event = AdditionalEvent::find($id);

        if (is_null($additional_event)) {
            return response()->json('additional_event_not_found', 404);
        }

        $additional_event->delete();

        return response()->json();
    }
}
