<?php

namespace App\Http\Controllers;

use App\Allowance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AllowanceController extends Controller
{
    public function index()
    {
        return Allowance::get();
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required|unique:allowances,id',
                'name' => 'required',
                'allowance' => 'required',
                'meal_allowance' => 'required',
            ]
        );

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data) {
                Allowance::create($user_data);
            });

            return response()->json([], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function get($id)
    {
        $allowance = Allowance::find($id);

        if (is_null($allowance)) {
            return response()->json('allowance_not_found', 404);
        }

        return $allowance;
    }

    public function update(Request $request, $id)
    {
        $allowance = Allowance::where([
            'is_from_server' => false,
            'id' => $id,
        ])->first();

        if (is_null($allowance)) {
            return response()->json('allowance_not_found', 404);
        }

        $this->validate(
            $request,
            [
                'name' => 'required',
                'allowance' => 'required',
                'meal_allowance' => 'required',
            ]
        );

        $user_data = $request->except('is_from_server');

        try {
            DB::transaction(function () use ($allowance, $user_data) {
                $allowance->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        $allowance = Allowance::where([
            'is_from_server' => false,
            'id' => $id,
        ])->first();

        if (is_null($allowance)) {
            return response()->json('allowance_not_found', 404);
        }

        $allowance->delete();

        return response()->json();
    }
}
