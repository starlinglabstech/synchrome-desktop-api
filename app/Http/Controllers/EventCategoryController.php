<?php

namespace App\Http\Controllers;

use App\EventCategory;

class EventCategoryController extends Controller
{
    public function index()
    {
        return EventCategory::get();
    }
}
