<?php

namespace App\Http\Controllers;

use App\Calendar;

class CalendarController extends Controller
{
    public function index()
    {
        return Calendar::with(['events', 'events.category'])->get();
    }

    public function get($id)
    {
        $calendar = Calendar::with('events')->find($id);

        if (is_null($calendar)) {
            return response()->json('calendar_not_found', 404);
        }

        return $calendar;
    }
}
