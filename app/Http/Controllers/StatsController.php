<?php

namespace App\Http\Controllers;

use App\Allowance;
use App\Rank;

class StatsController extends Controller
{
    public function allowances()
    {
        return Allowance::with('employees')
            ->orderBy('name', 'ASC')
            ->get();
    }

    public function ranks()
    {
        return Rank::with('employees')
            ->orderBy('name', 'ASC')
            ->get();
    }
}
