<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class SyncController extends Controller
{
    public function run()
    {
        Artisan::call('synchrome:sync');

        return response()->json();
    }
}
