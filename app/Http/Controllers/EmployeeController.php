<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Helpers\ScanLogHelper;
use App\Template;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        return Employee::with([
            'agency' => function ($query) {
                $query->select(['id', 'name']);
            },
            'echelon' => function ($query) {
                $query->select(['id', 'name']);
            },
            'rank' => function ($query) {
                $query->select(['id', 'name']);
            },
            'religion' => function ($query) {
                $query->select(['id', 'name']);
            },
            'calendar' => function ($query) {
                $query->select(['id', 'name']);
            },
            'workshift' => function ($query) {
                $query->select(['id', 'name']);
            },
            'templates',
            'allowance',
        ])
        ->where('agency_id', env('AGENCY_ID'))
        ->get();
    }

    public function get($id)
    {
        $employee = Employee::with([
            'agency' => function ($query) {
                $query->select(['id', 'name']);
            },
            'echelon' => function ($query) {
                $query->select(['id', 'name']);
            },
            'rank' => function ($query) {
                $query->select(['id', 'name']);
            },
            'religion' => function ($query) {
                $query->select(['id', 'name']);
            },
            'calendar' => function ($query) {
                $query->select(['id', 'name']);
            },
            'workshift' => function ($query) {
                $query->select(['id', 'name']);
            },
            'templates',
            'allowance',
        ])->find($id);

        if (is_null($employee)) {
            return response()->json('employee_not_found', 404);
        }

        return $employee;
    }

    protected function fetchFromDevice()
    {
        $helper = new ScanLogHelper;

        return collect($helper->fetchEmployees()->getPayload());
    }

    public function syncTemplates(Request $request, $id)
    {
        $employee = Employee::find($id);

        if (is_null($employee)) {
            return response()->json('employee_not_found', 404);
        }

        $this->validate(
            $request,
            [
                'dummy_employee_id' => 'required',
            ]
        );

        $dummy_employee = $this->fetchFromDevice()
            ->where('id', $request->get('dummy_employee_id'))
            ->first();

        if (is_null($dummy_employee)) {
            return response()->json('dummy_employee_not_found', 404);
        }

        $templates = collect($dummy_employee['templates'])->map(function ($template) {
            return new Template($template);
        });

        try {
            DB::transaction(function () use ($employee, $templates) {
                $employee->templates()->delete();
                $employee->templates()->saveMany($templates);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
