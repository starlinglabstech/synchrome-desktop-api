<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\File;

class FileController extends Controller
{
    /**
     * Uploads a file.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            ['file' => 'required|file']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $payload = $request->file('file');
            $extension = $payload->getClientOriginalExtension();
            $basename_length = strlen($payload->getClientOriginalName()) - (strlen($extension) + 1);
            $basename = substr($payload->getClientOriginalName(), 0, $basename_length);
            $file_name = File::sanitizeName($basename) . '.' . $payload->getClientOriginalExtension();

            DB::transaction(function () use (
                $file_name,
                $payload,
                &$file
            ) {
                $path = 'files/' . $file_name;
                
                Storage::disk('local')->put($path, $payload);

                $file = File::create([
                    'name' => $file_name,
                    'path' => $path,
                ]);
            });

            return response()->json($file, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
