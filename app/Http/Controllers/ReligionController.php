<?php

namespace App\Http\Controllers;

use App\Religion;

class ReligionController extends Controller
{
    public function index()
    {
        return Religion::get();
    }
}
