<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        return User::with('role')->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_conf' => 'required|same:password',
            'role_id' => 'required',
        ]);

        $user_data = $request->all();
        $user_data['password'] = app('hash')->make($user_data['password']);
        unset($user_data['password_conf']);

        try {
            DB::transaction(function () use ($user_data) {
                User::create($user_data);
            });

            return response()->json([], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function get($id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'role_id' => 'required',
            'password' => 'sometimes|required',
            'password_conf' => 'required_with:password|same:password',
        ]);

        $user_data = $request->except('password_conf');

        if (!empty($user_data['password'])) {
            $user_data['password'] = app('hash')->make($user_data['password']);
        }

        try {
            DB::transaction(function () use ($user, $user_data) {
                $user->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }

        $user->delete();

        return response()->json();
    }
}
