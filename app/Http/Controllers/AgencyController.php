<?php

namespace App\Http\Controllers;

use App\Agency;

class AgencyController extends Controller
{
    public function index()
    {
        return Agency::get();
    }

    public function echelons($id)
    {
        $agency = Agency::with('echelons')->find($id);

        if (is_null($agency)) {
            return response()->json('agency_not_found', 404);
        }

        return $agency->echelons;
    }
}
