<?php

namespace App\Http\Controllers;

use App\Rank;

class RankController extends Controller
{
    public function index()
    {
        return Rank::get();
    }
}
