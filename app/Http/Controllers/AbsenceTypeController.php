<?php

namespace App\Http\Controllers;

use App\AbsenceType;

class AbsenceTypeController extends Controller
{
    public function index()
    {
        return AbsenceType::parentOnly()
            ->with(['children'])->get();
    }
}
