<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required',
                'password' => 'required',
            ]
        );

        $user = User::with('role')->where('email', $request->get('email'))->first();

        if (is_null($user)) {
            return response()->json('wrong_credentials', 422);
        }

        if (!password_verify($request->get('password'), $user->password)) {
            return response()->json('wrong_credentials', 422);
        }

        return response()->json($this->generateJwt($user));
    }

    protected function generateJwt(User $user)
    {
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuedAt(time())
            ->set('user', $user)
            ->sign($signer, env('INTERNAL_JWT_SIGN_KEY'))
            ->getToken();

        return (string) $token;
    }

    public function changePassword(Request $request)
    {
        $user = User::find($request->user->id);

        $this->validate(
            $request,
            [
                'password' => 'required',
                'new_password' => 'required',
                'new_password_conf' => 'required|same:new_password',
            ]
        );

        if (!password_verify($request->get('password'), $user->password)) {
            return response()->json('password_not_match', 422);
        }

        try {
            DB::transaction(function () use ($user, $request) {
                $user->update(['password' => app('hash')->make($request->get('new_password'))]);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
