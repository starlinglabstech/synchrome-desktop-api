<?php

namespace App\Http\Middleware;

use App\Key;
use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Authenticate
{
    public function handle($request, Closure $next)
    {
        if (empty($request->header('Authorization'))) {
            return response()->json('token_required', 401);
        }

        $token = $this->validateToken(explode(' ', $request->header('Authorization'))[1]);

        if (!is_null($token)) {
            $user = $token->getClaim('user');

            if (is_null($user)) {
                response()->json('invalid_token', 401);
            }

            $request->user = $user;

            return $next($request);
        }

        return response()->json('invalid_token', 401);
    }

    public function validateToken($token)
    {
        $token = (new Parser)->parse($token);
        $signer = new Sha256;

        if ($token->verify($signer, env('INTERNAL_JWT_SIGN_KEY'))) {
            return $token;
        }

        return null;
    }
}
