<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Calendar extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'start',
        'end',
        'active',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function eventAt(Carbon $date, $event_category_id = 1)
    {
        return $this->events
            ->filter(function ($event) use ($date, $event_category_id) {
                if (is_null($event->end)) {
                    return $date->isSameDay($event->start) && $event->event_category_id === $event_category_id;
                }

                return $date->gte($event->start) && $date->lte($event->end) && $event->event_category_id === $event_category_id;
            });
    }
}
