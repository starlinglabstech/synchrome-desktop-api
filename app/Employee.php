<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Employee extends Model
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string',
        'married' => 'boolean',
    ];

    public $incrementing = false;

    protected $fillable = [
        'id',
        'agency_id',
        'echelon_id',
        'rank_id',
        'workshift_id',
        'calendar_id',
        'allowance_id',
        'religion_id',
        'name',
        'gender',
        'married',
        'phone',
        'address',
        'last_device_sync',
    ];

    protected $hidden = [
        'agency_id',
        'echelon_id',
        'rank_id',
        'workshift_id',
        'calendar_id',
        'allowance_id',
        'religion_id',
    ];

    protected $dates = [
        'deleted_at',
        'last_device_sync',
    ];

    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    public function echelon()
    {
        return $this->belongsTo(Echelon::class);
    }

    public function rank()
    {
        return $this->belongsTo(Rank::class);
    }

    public function workshift()
    {
        return $this->belongsTo(Workshift::class);
    }

    public function calendar()
    {
        return $this->belongsTo(Calendar::class);
    }

    public function allowance()
    {
        return $this->belongsTo(Allowance::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class);
    }

    public function templates()
    {
        return $this->hasMany(Template::class);
    }

    public function additionalEvents()
    {
        return $this->belongsToMany(AdditionalEvent::class);
    }

    public function isOffAt(Carbon $date)
    {
        $events_on_date = $this->calendar->events()->get()
            ->filter(function ($event) {
                return $event->event_category_id === 1;
            })
            ->filter(function ($event) use ($date) {
                if (is_null($event->end)) {
                    return $date->isSameDay($event->start);
                }

                return $date->gte($event->start) && $date->lte($event->end);
            })
            ->all();
        
        $additional_events_on_date = $this->additionalEvents
            ->filter(function ($event) {
                return $event->event_category_id === 1;
            })
            ->filter(function ($event) use ($date) {
                if (is_null($event->end)) {
                    return $date->isSameDay($event->start);
                }

                return $date->gte($event->start) && $date->lte($event->end);
            })
            ->all();

        return !empty($events_on_date) || !empty($additional_events_on_date);
    }

    public static function generatePin()
    {
        $pin = mt_rand(1, 9999999);

        if (self::where('pin', $pin)->count() > 0) {
            return self::generatePin();
        }

        return $pin;
    }

    public static function byPin($pin)
    {
        return self::where('pin', $pin)->first();
    }

    public function scanLogs()
    {
        return $this->hasMany(ScanLog::class);
    }

    public function attendancesInRange(Carbon $start, Carbon $end)
    {
        return $this->scanLogs
            ->filter(function ($scan_log) use ($start, $end) {
                return $scan_log->date->gte($start) && $scan_log->date->lte($end) && $scan_log->status;
            })
            ->count();
    }

    public function absencesInRange(Carbon $start, Carbon $end)
    {
        $total = 0;

        $absences['types'] = AbsenceType::get()
            ->map(function ($absence_type) use (&$total) {
                $count = $this->scanLogs
                            ->filter(function ($scan_log) use ($absence_type) {
                                return $scan_log->absence_type_id === $absence_type->id;
                            })
                            ->count();
                $total += $count;

                return [
                    'id' => $absence_type->id,
                    'count' => $count,
                ];
            })
            ->all();

        $absences['total'] = $total;
        
        return $absences;
    }

    public function allowanceInRange(Carbon $start, Carbon $end)
    {
        $scan_logs = $this->scanLogs
            ->filter(function ($scan_log) use ($start, $end) {
                return $scan_log->date->gte($start) && $scan_log->date->lte($end);
            });

        if ($scan_logs->count() === 0) {
            return [
                'allowance' => $this->allowance->allowance,
                'deduction' => $this->allowance->allowance,
            ];
        }

        $workshift_work_duration = $scan_logs->sum('workshift_work_duration');
        $work_duration = $scan_logs->sum('work_duration');

        return [
            'allowance' => $this->allowance->allowance,
            'deduction' => $this->allowance->allowance - (($work_duration / $workshift_work_duration) * $this->allowance->allowance),
        ];
    }

    public function mealAllowanceInRange(Carbon $start, Carbon $end)
    {
        $count = $this->scanLogs
            ->filter(function ($scan_log) use ($start, $end) {
                return $scan_log->date->gte($start) && $scan_log->date->lte($end) && $scan_log->status;
            })
            ->count();

        return ['allowance' => $this->allowance->meal_allowance * $count];
    }

    public function isWorkingAt(Carbon $date)
    {
        return !is_null(
            $this->workshift->details
                ->where('day', $date->format('D'))
                ->where('active', true)
                ->first()
        ) && !$this->isOffAt($date);
    }

    public function overWorkshiftTime()
    {
        $now = strtotime(date('H:i:s'));
        $workshift_out = strtotime($this->workshift->details->where('day', date('D'))->first()->out);

        return $now > $workshift_out;
    }
}
