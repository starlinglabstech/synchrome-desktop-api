<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Helpers\ScanLogHelper;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SyncCommand::class,
        Commands\TruncateSyncablesCommand::class,
        Commands\GenerateScanLogsCommand::class,
        Commands\MergeScanLogsCommand::class,
        Commands\SyncEmployeesToDeviceCommand::class,
        Commands\TruncateEmployeesOnDeviceCommand::class,
        Commands\PingDeviceCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $scan_log_helper = new ScanLogHelper;

        $schedule
            ->call(function () use ($scan_log_helper) {
                $scan_log_helper->generateScanLogs(Carbon::now());
            })
            ->hourly();

        $schedule
            ->call(function () use ($scan_log_helper) {
                $scan_log_helper->mergeScanLogs(Carbon::now());
            })
            ->hourly()
            ->between('06:00', '17:00');
    }
}
