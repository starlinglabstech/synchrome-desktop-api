<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\SyncHelper;

class SyncCommand extends Command
{
    protected $signature = 'synchrome:sync';

    protected $description = 'Sync resources from/to server';

    protected $helper;

    public function __construct()
    {
        parent::__construct();

        $this->helper = new SyncHelper;
    }

    public function handle()
    {
        try {
            $this->helper->sync();
            $this->info('Sync completed');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}