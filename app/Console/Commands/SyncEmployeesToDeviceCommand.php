<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\TemplateHelper;

class SyncEmployeesToDeviceCommand extends Command
{
    protected $signature = 'device:sync {--reset}';

    protected $description = 'Sync employees to device';

    protected $helper;

    public function __construct()
    {
        parent::__construct();

        $this->helper = new TemplateHelper;
    }

    public function handle()
    {
        try {
            $this->helper->sync($this->option('reset'));
            $this->info('Sync completed');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}