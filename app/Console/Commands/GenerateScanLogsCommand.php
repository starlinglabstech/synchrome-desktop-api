<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ScanLogHelper;
use Carbon\Carbon;

class GenerateScanLogsCommand extends Command
{
    protected $signature = 'scan-log:generate {date : Date where scan logs generated for}';

    protected $description = 'Generate scan logs for specific date';

    protected $helper;

    public function __construct()
    {
        parent::__construct();

        $this->helper = new ScanLogHelper;
    }

    public function handle()
    {
        try {
            $date = Carbon::createFromFormat('Y-m-d', $this->argument('date'));

            if ($this->helper->generateScanLogs($date)) {
                $this->info('Scan logs generated');
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}