<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TruncateSyncablesCommand extends Command
{
    protected $signature = 'synchrome:truncate';

    protected $description = 'Truncate sync-able tables';

    protected $tables = [
        'templates',
        'employees',
        'ranks',
        'shifts',
        'workshifts',
        'absence_types',
        'allowances',
        'events',
        'calendars',
        'echelons',
        'agencies'
    ];

    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}