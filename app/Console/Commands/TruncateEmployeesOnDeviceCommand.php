<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\TemplateHelper;
use Dewadg\FmConnector\Connector;

class TruncateEmployeesOnDeviceCommand extends Command
{
    protected $signature = 'device:truncate';

    protected $description = 'Remove all employees on device';

    protected $connector;

    public function __construct()
    {
        parent::__construct();

        $this->connector = new Connector(
            'easy_link',
            [
                'serial_number' => env('DEVICE_SERIAL_NUMBER'),
                'base_uri' => env('DEVICE_HOST'),
            ]
        );
    }

    public function handle()
    {
        try {
            $this->connector->destroyUsers();
            $this->info('Employees on device truncated');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}