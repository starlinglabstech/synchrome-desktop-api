<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ScanLogHelper;
use Carbon\Carbon;

class MergeScanLogsCommand extends Command
{
    protected $signature = 'scan-log:merge {date : Date where scan logs generated for}';

    protected $description = 'Merge scan logs from device for specific date';

    protected $helper;

    public function __construct()
    {
        parent::__construct();

        $this->helper = new ScanLogHelper;
    }

    public function handle()
    {
        try {
            $date = Carbon::createFromFormat('Y-m-d', $this->argument('date'));

            if ($this->helper->mergeScanLogs($date)) {
                $this->info('Scan logs merged');
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}