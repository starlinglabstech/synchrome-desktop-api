<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\TemplateHelper;
use Dewadg\FmConnector\Connector;

class PingDeviceCommand extends Command
{
    protected $signature = 'device:ping';

    protected $description = 'Ping to device';

    protected $connector;

    public function __construct()
    {
        parent::__construct();

        $this->connector = new Connector(
            'easy_link',
            [
                'serial_number' => env('DEVICE_SERIAL_NUMBER'),
                'base_uri' => env('DEVICE_HOST'),
            ]
        );
    }

    public function handle()
    {
        try {
            $this->connector->getDeviceInfo();
            $this->info('Ping succeeded');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}