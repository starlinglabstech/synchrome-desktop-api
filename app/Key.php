<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Key extends Model
{
    use SoftDeletes;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = [
        'cluster_id',
        'key',
        'active',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public static function generate()
    {
        $key = str_random(32);

        if (Key::where('key', $key)->count() > 0) {
            return self::generate();
        }

        return $key;
    }

    public static function isValid($key)
    {
        if (is_null(Key::where(['key' => $key, 'active' => true])->first())) {
            return false;
        }

        return true;
    }

    public static function findByKey($key)
    {
        return Key::where('key', $key)->first();
    }
}
