<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model
{
    use SoftDeletes;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = [
        'id',
        'workshift_id',
        'day',
        'in',
        'out',
        'active'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = ['workshift_id'];
}
