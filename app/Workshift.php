<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Workshift extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function details()
    {
        return $this->hasMany(Shift::class);
    }

    public function isSaturdayWorkday()
    {
        return $this->details->where('day', 'Sat')->count() === 1;
    }

    public function today()
    {
        return $this->details->where('day', date('D'))->first();
    }

    public function atDate(Carbon $date)
    {
        return $this->details->where('day', $date->format('D'))->first();
    }
    
    public function workdaysInRange(Calendar $calendar, Carbon $start, Carbon $end)
    {
        $workshift_workdays = $this->details
            ->filter(function ($detail) {
                return $detail->active;
            })
            ->map(function ($detail) {
                return $detail->day;
            })
            ->all();

        $calendar_holiday_dates = $calendar->events
            ->filter(function ($event) {
                return $event->event_category_id === 1;
            })
            ->map(function ($event) {
                if (is_null($event->end)) {
                    return $event->start->format('Y-m-d');
                }

                return $this->datesBetween($event->start, $event->end);
            })
            ->flatten()
            ->all();

        $num_of_workdays = $start->diffInDaysFiltered(
            function (Carbon $date) use ($workshift_workdays, $calendar_holiday_dates) {
                return in_array($date->format('D'), $workshift_workdays) && !in_array($date->format('Y-m-d'), $calendar_holiday_dates);
            },
            $end
        );

        return $num_of_workdays + 1;
    }

    protected function datesBetween(Carbon $start, Carbon $end)
    {
        $dates = [];

        for ($i = $start; $i->lte($end); $i->addDay()) {
            array_push($dates, $i->format('Y-m-d'));
        }

        return $dates;
    }
}
