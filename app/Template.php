<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'employee_id',
        'template',
        'index',
        'alg_ver',
    ];

    protected $hidden = ['employee_id'];
}
