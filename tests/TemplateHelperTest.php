<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Helpers\TemplateHelper;

class TemplateHelperTest extends TestCase
{
    public function testSync()
    {
        $helper = new TemplateHelper;
        
        $this->assertTrue($helper->sync(true));
    }
}
