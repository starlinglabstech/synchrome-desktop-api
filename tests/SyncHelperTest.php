<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Helpers\SyncHelper;

class SyncHelperTest extends TestCase
{
    public function testSync()
    {
        $helper = new SyncHelper;
        
        $this->assertTrue($helper->sync());
    }
}
