<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Employee;
use Carbon\Carbon;

class EmployeeTest extends TestCase
{
    /**
     * Test data is based on SMKN 1 Denpasar and Kalender Provinsi 2017/2018 data.
     * 
     * @return void
     */
    public function testWorkshiftWorkdaysInRange()
    {
        $employee = Employee::find('195612311981031256');
        $start = Carbon::createFromFormat('Y-m-d', '2017-09-01');
        $end = Carbon::createFromFormat('Y-m-d', '2017-09-30');
        $workdays = $employee->workshift->workdaysInRange($employee->calendar, $start, $end);

        $this->assertTrue($workdays === 24);
    }

    public function testAbsencesInRange()
    {
        $employee = Employee::find('195612311981031256');
        $start = Carbon::createFromFormat('Y-m-d', '2017-09-01');
        $end = Carbon::createFromFormat('Y-m-d', '2017-09-30');

        $this->assertTrue(is_array($employee->absencesInRange($start, $end)));
    }

    public function testAllowanceInRange()
    {
        $employee = Employee::find('195612311981031256');
        $start = Carbon::createFromFormat('Y-m-d', '2017-11-01');
        $end = Carbon::createFromFormat('Y-m-d', '2017-11-30');
        $allowance = $employee->allowanceInRange($start, $end);

        $this->assertTrue($allowance['allowance'] == 1000000 && $allowance['deduction'] == 1000000);
    }

    public function testOffDay()
    {
        $employee = Employee::find('195612311981031256');
        $date = Carbon::createFromFormat('Y-m-d', '2017-11-23');

        $this->assertTrue($employee->isOffAt($date));
    }

    public function testWorkday()
    {
        $employee = Employee::find('195612311981031256');
        $date = Carbon::createFromFormat('Y-m-d', '2017-11-20');

        $this->assertTrue($employee->isWorkingAt($date));
    }

    public function testOverWorkshiftTime()
    {
        $employee = Employee::find('195612311981031256');

        $this->assertTrue($employee->overWorkshiftTime());
    }
}
