<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Helpers\ScanLogHelper;

class ScanLogHelperTest extends TestCase
{
    public function testSync()
    {
        $helper = new ScanLogHelper;
        
        $this->assertTrue($helper->generateScanLogs());
    }

    public function testFetch()
    {
        $helper = new ScanLogHelper;

        $this->assertTrue(is_array($helper->fetchByDate(date('Y-m-d'))));
    }

    public function testMerge()
    {
        $helper = new ScanLogHelper;

        $this->assertTrue($helper->mergeScanLogs(date('Y-m-d')));
    }
}
