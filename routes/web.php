<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return response()->json('Synchrome Desktop API v0.1');
});
$router->post('login', 'AuthController@login');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('sync', 'SyncController@run');
    $router->post('change-password', 'AuthController@changePassword');

    $router->get('roles', 'RoleController@index');
    $router->get('users', 'UserController@index');
    $router->post('users', 'UserController@store');
    $router->get('users/{id}', 'UserController@get');    
    $router->patch('users/{id}', 'UserController@update');
    $router->delete('users/{id}', 'UserController@destroy');

    $router->post('files', 'FileController@upload');

    $router->get('event-categories', 'EventCategoryController@index');
    $router->get('calendars', 'CalendarController@index');
    $router->get('calendars/{id}', 'CalendarController@get');
    $router->get('calendars/{id}', 'CalendarController@get');
    $router->get('absence-types', 'AbsenceTypeController@index');
    $router->get('additional-events', 'AdditionalEventController@index');
    $router->post('additional-events', 'AdditionalEventController@store');
    $router->get('additional-events/{id}', 'AdditionalEventController@get');
    $router->patch('additional-events/{id}', 'AdditionalEventController@update');
    $router->delete('additional-events/{id}', 'AdditionalEventController@destroy');
    $router->get('permits/{file_name}', 'PermitController@get');

    $router->get('ranks', 'RankController@index');
    $router->get('religions', 'ReligionController@index');
    $router->get('agencies', 'AgencyController@index');
    $router->get('agencies/{id}/echelons', 'AgencyController@echelons');
    $router->get('workshifts', 'WorkshiftController@index');
    $router->get('workshifts/{id}', 'WorkshiftController@get');

    $router->get('employees', 'EmployeeController@index');
    $router->get('employees/{id}', 'EmployeeController@get');
    $router->post('employees/{id}/sync-templates', 'EmployeeController@syncTemplates');

    $router->get('allowances', 'AllowanceController@index');
    $router->post('allowances', 'AllowanceController@store');
    $router->patch('allowances/{id}', 'AllowanceController@update');
    $router->delete('allowances/{id}', 'AllowanceController@destroy');

    $router->get('reports/scan-log/{employee_id}', 'ReportController@scanLogByEmployee');
    $router->get('reports/allowance', 'ReportController@allowance');
    $router->get('reports/meal-allowance', 'ReportController@mealAllowance');

    $router->get('scan-logs', 'ScanLogController@index');
    $router->post('scan-logs/generate', 'ScanLogController@generateByDate');
    $router->post('scan-logs/merge', 'ScanLogController@mergeByDate');

    $router->get('stats/allowances', 'StatsController@allowances');
    $router->get('stats/ranks', 'StatsController@ranks');
});
